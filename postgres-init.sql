CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS users(
  uuid  UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  email TEXT NOT NULL UNIQUE CHECK (email <> ''::text),
  salt BYTEA NOT NULL,
  hash BYTEA NOT NULL
);

ALTER TABLE users OWNER TO doge;