package main

import (
	"fmt"

	"gitlab.com/cluttered-code/sso-go/src/server"
)

// @title SSO - Go
// @version 1.0
// @description Single Sign On - Go implementation. [Source Code](https://gitlab.com/cluttered-code/sso-go)

// @contact.name David Clutter
// @contact.email cluttered.code@gmail.com

// @license.name ISC
// @license.url https://choosealicense.com/licenses/isc/

// @host sso.cluttered-code.com:6060
// @BasePath /v0
func main() {
	fmt.Println("######## SSO ########")
	server.Start(6060)
}
