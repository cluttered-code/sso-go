package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cluttered-code/sso-go/src/server/controllers"
)

func AddTokenRoutes(router *gin.RouterGroup) {
	tokenGroup := router.Group("tokens")
	{
		tokenGroup.GET("public-key", controllers.Token.PublicKey)
		tokenGroup.POST("jwt", controllers.Token.JWT)
	}
}
