package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cluttered-code/sso-go/src/server/controllers"
)

func AddUserRoutes(router *gin.RouterGroup) {
	userGroup := router.Group("users")
	{
		userGroup.POST("", controllers.User.Create)
		userGroup.GET("", controllers.User.List)
		userGroup.DELETE(":uuid", controllers.User.Delete)
	}
}
