package response

type JWT struct {
	Token string `json:"token" binding:"required"`
	Type  string `json:"type" binding:"required" example:"Bearer"`
}
