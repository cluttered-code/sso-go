package request

type Credentials struct {
	Email    string `json:"email" binding:"required" example:"nobody@nowhere.com"`
	Password string `json:"password" binding:"required" example:"$uper$ecret"`
}
