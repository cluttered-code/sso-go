package server

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/cluttered-code/sso-go/src/docs"
	"gitlab.com/cluttered-code/sso-go/src/server/routes"
)

var V0Router *gin.Engine

func init() {
	V0Router = gin.Default()
	V0Router.RedirectTrailingSlash = false

	V0Router.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
	}))

	v0 := V0Router.Group("v0")
	{
		routes.AddUserRoutes(v0)
		routes.AddTokenRoutes(v0)
	}

	V0Router.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, "pong")
		return
	})

	V0Router.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusPermanentRedirect, "/swagger/index.html")
	})

	V0Router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
