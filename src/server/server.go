package server

import (
	"fmt"
)

func Start(port int) {
	fmt.Printf("starting server on port %d\n", port)
	V0Router.Run(fmt.Sprintf(":%d", port))
}
