package controllers

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/cluttered-code/sso-go/src/utils"

	"github.com/gin-gonic/gin"
	"gitlab.com/cluttered-code/sso-go/src/database/models"
	"gitlab.com/cluttered-code/sso-go/src/database/repositories"
	"gitlab.com/cluttered-code/sso-go/src/server/models/request"
	"gitlab.com/cluttered-code/sso-go/src/server/models/response"
)

var Token = new(tokenController)

type tokenController struct{}

//
// @Summary Public Key
// @Description obtain public key for local JWT validation
// @Tags Tokens
// @Accept  json
// @Produce  json
// @Success 200 {string} string "ok"
// @Router /tokens/public-key [get]
func (tc tokenController) PublicKey(c *gin.Context) {
	c.JSON(http.StatusOK, os.Getenv(`RSA_PUBLIC`))
}

//
// @Summary Obtain JWT
// @Description Login to obtain JWT
// @Tags Tokens
// @Accept  json
// @Produce  json
// @Param credentials body request.Credentials true "user credentials"
// @Success 200 {object} response.JWT "OK"
// @Router /tokens/jwt [post]
func (tokenController) JWT(c *gin.Context) {
	credentials := new(request.Credentials)
	err := c.BindJSON(credentials)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"code":    http.StatusBadRequest,
			"message": fmt.Sprint(err),
		})
		return
	}

	userCh := make(chan *models.User)
	defer close(userCh)
	go repositories.User.FindByEmail(credentials.Email, userCh)
	user := <-userCh
	if user == nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"code":    http.StatusUnauthorized,
			"message": "invalid email or password",
		})
		return
	}

	verified := utils.VerifyPassword(credentials.Password, user.Salt, user.Hash)
	if !verified {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"code":    http.StatusUnauthorized,
			"message": "invalid email or password",
		})
		return
	}

	jwtCh := make(chan string)
	defer close(jwtCh)
	go utils.JWT.Create(user, jwtCh)
	token := <-jwtCh

	jwt := new(response.JWT)
	jwt.Token = token
	jwt.Type = "Bearer"

	c.JSON(http.StatusOK, jwt)
}
