package controllers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cluttered-code/sso-go/src/database/models"
	"gitlab.com/cluttered-code/sso-go/src/database/repositories"
	"gitlab.com/cluttered-code/sso-go/src/server/models/request"
)

var User = new(userController)

type userController struct{}

//
// @Summary List Users
// @Description list all current users
// @Tags Users
// @Accept  json
// @Produce  json
// @Success 200 {array} models.User "ok"
// @Router /users [get]
func (uc userController) List(c *gin.Context) {
	ch := make(chan []*models.User)
	defer close(ch)
	go repositories.User.List(ch)
	users := <-ch

	c.JSON(http.StatusOK, users)
}

//
// @Summary Create User
// @Description create a new user
// @Tags Users
// @Accept  json
// @Produce  json
// @Param credentials body request.Credentials true "user credentials"
// @Success 201 {object} models.User "Created"
// @Router /users [post]
func (uc userController) Create(c *gin.Context) {
	credentials := new(request.Credentials)
	err := c.BindJSON(credentials)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"code":    http.StatusBadRequest,
			"message": fmt.Sprint(err),
		})
		return
	}

	ch := make(chan *models.User)
	defer close(ch)
	go repositories.User.Insert(credentials, ch)
	user := <-ch

	if user == nil {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{
			"code":    http.StatusConflict,
			"message": "User already exists",
		})
		return
	}

	c.Header("location", fmt.Sprintf("/v0/users/%s", user.UUID))
	c.JSON(http.StatusCreated, user)
}

//
// @Summary Delete User
// @Description delete a user by uuid
// @Tags Users
// @Accept  json
// @Produce  json
// @Param uuid path string true "user credentials"
// @Success 204 "no content"
// @Router /users/{uuid} [delete]
func (uc userController) Delete(c *gin.Context) {
	uuid := c.Param(`uuid`)
	repositories.User.Delete(uuid)

	c.JSON(http.StatusNoContent, nil)
}
