package utils

import (
	"crypto/rand"
	"crypto/sha256"

	"golang.org/x/crypto/pbkdf2"
)

const (
	saltLength     = 80
	hashLength     = 64
	hashIterations = 64000
)

func GenerateSaltAndHash(password string) ([]byte, []byte) {
	passwordBytes := []byte(password)
	salt := generateSalt()
	ch := make(chan []byte)
	go generateHash(passwordBytes, salt, ch)
	hash := <-ch
	return salt, hash
}

func VerifyPassword(password string, salt []byte, hash []byte) bool {
	passwordBytes := []byte(password)
	ch := make(chan []byte)
	go generateHash(passwordBytes, salt, ch)
	passwordHash := <-ch
	return timeSafeEquals(passwordHash, hash)
}

func generateSalt() []byte {
	salt := make([]byte, saltLength)
	_, _ = rand.Read(salt)
	return salt
}

func generateHash(password []byte, salt []byte, ch chan []byte) {
	ch <- pbkdf2.Key(password, salt, hashIterations, hashLength, sha256.New)
	close(ch)
}

func timeSafeEquals(a []byte, b []byte) bool {
	equal := true
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			equal = false
		}
	}
	return equal
}
