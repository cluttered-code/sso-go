package utils

import (
	"crypto/rsa"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/cluttered-code/sso-go/src/database/models"
)

var JWT = new(jsonwebtoken)

type jsonwebtoken struct{}

var privateKey *rsa.PrivateKey
var publicKey *rsa.PublicKey

func init() {
	publicKey = parsePublicKey()
	privateKey = parsePrivateKey()
}

func parsePrivateKey() *rsa.PrivateKey {
	privateKeyStr := os.Getenv("RSA_PRIVATE")
	privateKeyBytes := []byte(privateKeyStr)
	privateKeyRsa, err := jwt.ParseRSAPrivateKeyFromPEM(privateKeyBytes)
	if err != nil {
		panic(`Failed to load Private Key`)
	}
	return privateKeyRsa
}

func parsePublicKey() *rsa.PublicKey {
	publicKeyStr := os.Getenv("RSA_PUBLIC")
	publicKeyBytes := []byte(publicKeyStr)
	publicKeyRsa, err := jwt.ParseRSAPublicKeyFromPEM(publicKeyBytes)
	if err != nil {
		panic(`Failed to load Public Key`)
	}
	return publicKeyRsa
}

func (j jsonwebtoken) Create(user *models.User, ch chan string) {
	currentTime := time.Now()
	currentUnixTime := currentTime.Unix()
	expireUnixTime := currentTime.Add(time.Hour * 24).Unix()
	claims := &jwt.StandardClaims{
		Issuer:    "sso-go",
		IssuedAt:  currentUnixTime,
		NotBefore: currentUnixTime,
		ExpiresAt: expireUnixTime,
		Subject:   user.UUID,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	jwt, err := token.SignedString(privateKey)
	if err != nil {
		panic(`Error signing JWT`)
	}
	ch <- jwt
}

func (j jsonwebtoken) Decode(jwtStr string) (*jwt.Token, error) {
	return jwt.Parse(jwtStr, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
}
