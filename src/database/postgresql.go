package database

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/lib/pq" // loads pg driver
)

// PostgreSQL - global singeton DB access
var PostgreSQL *sql.DB

func init() {
	fmt.Println("connecting to PostgreSQL...")
	host := os.Getenv(`POSTGRES_HOST`)
	port := 5432
	user := os.Getenv(`POSTGRES_USER`)
	password := os.Getenv(`POSTGRES_PASSWORD`)
	dbname := os.Getenv(`POSTGRES_DB`)
	connTemplate := "postgres://%s:%s@%s:%d/%s?sslmode=disable"
	connStr := fmt.Sprintf(connTemplate, user, password, host, port, dbname)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		panic("Failed to open database connection")
	}
	PostgreSQL = db
}
