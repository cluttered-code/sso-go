package repositories

import (
	"gitlab.com/cluttered-code/sso-go/src/database"
	"gitlab.com/cluttered-code/sso-go/src/database/models"
	"gitlab.com/cluttered-code/sso-go/src/server/models/request"
	"gitlab.com/cluttered-code/sso-go/src/utils"
)

var User = new(userRepo)
var db = database.PostgreSQL

type userRepo struct{}

func (ur userRepo) List(ch chan []*models.User) {
	sql := "SELECT * from users"
	rows, err := db.Query(sql)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	users := []*models.User{}
	for rows.Next() {
		user, _ := new(models.User).FromRows(rows)
		if user != nil {
			users = append(users, user)
		}
	}

	ch <- users
}

func (ur userRepo) Insert(credentials *request.Credentials, ch chan *models.User) {
	sql := "INSERT INTO users(email,salt,hash) VALUES ($1,$2,$3) RETURNING *"
	salt, hash := utils.GenerateSaltAndHash(credentials.Password)
	row := db.QueryRow(sql, credentials.Email, salt, hash)
	user, _ := new(models.User).FromRow(row)
	ch <- user
}

func (ur userRepo) FindByEmail(email string, ch chan *models.User) {
	sql := "SELECT * FROM users WHERE email=$1 LIMIT 1"
	row := db.QueryRow(sql, email)
	user, _ := new(models.User).FromRow(row)
	ch <- user
}

func (ur userRepo) Delete(uuid string) {
	sql := "DELETE FROM users WHERE uuid=$1"
	_, err := db.Exec(sql, uuid)
	if err != nil {
		panic(err)
	}
}
