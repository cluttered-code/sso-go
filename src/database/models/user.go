package models

import "database/sql"

type User struct {
	UUID  string `json:"uuid" example:"00000000-0000-0000-0000-000000000000"`
	Email string `json:"email" example:"nobody@nowhere.com"`
	Salt  []byte `json:"-"`
	Hash  []byte `json:"-"`
}

func (user User) FromRow(row *sql.Row) (*User, error) {
	err := row.Scan(&user.UUID, &user.Email, &user.Salt, &user.Hash)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func (user User) FromRows(rows *sql.Rows) (*User, error) {
	err := rows.Scan(&user.UUID, &user.Email, &user.Salt, &user.Hash)
	if err != nil {
		return nil, err
	}
	return &user, nil
}
