# Single Sign On - Go

## Live Demo
http://sso.cluttered-code.com:6060

## Development

### Install
```shell script
docker-compose up -d --build --force-recreate
```

**Port Mappings**
* localhost:60606 -> sso-go:6060
* localhost:54325 -> sso-postgres:5432

## Table of Contents
- General Endpoints
  - [Ping](#ping)
- User Endpoints
  - [List Users](#list-users)
  - [Create User](#create-user)
  - [Delete User](#delete-user)
- Token Endpoints
  - [Public Key](#public-key)
  - [JWT](#jwt)

### Ping

Test if server is responding.

|Method|URL|Body|
|:---:|---|:---:|
|GET|/ping|N/A|

<details>
<summary><b>Response</b></summary>

```js
"pong"
```

</details>

### List Users

List all the current users.

|Method|URL|Body|
|:---:|---|:---:|
|GET|/v0/users|N/A|

<details>
<summary><b>Response</b></summary>

```js
[
  {
    "uuid": "00000000-0000-0000-0000-000000000000",
    "email": "nobody@nowhere.com"
  },
  {
    "uuid": "11111111-1111-1111-1111-111111111111",
    "email": "everybody@everywhere.com"
  }
]
```

</details>

### Create User

Create a new user.

|Method|URL|Body|
|:---:|---|:---:|
|POST|/v0/users|{email: string, password: string}|

<details>
<summary><b>Response</b></summary>

```js
{
  "uuid": "00000000-0000-0000-0000-000000000000",
  "email": "nobody@nowhere.com"
}
```

</details>

### Delete User

Remove a user.

|Method|URL|Body|
|:---:|---|:---:|
|DELETE|/v0/users/{uuid}|N/A|

<details>
<summary><b>Response</b></summary>

```js
NO CONTENT
```

</details>

### Public Key

Obtain the public key for JWT validation.

|Method|URL|Body|
|:---:|---|:---:|
|GET|/v0/tokens/public-key|N/A|

<details>
<summary><b>Response</b></summary>

```js
"-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSq..."
```

</details>

### JWT

Login and obtain JWT.

|Method|URL|Body|
|:---:|---|:---:|
|POST|/v0/tokens/jwt|{email: string, password: string}|

<details>
<summary><b>Response</b></summary>

```js
{
  "token": "eyJhbGciOiJSU...",
  "type": "Bearer"
}
```

</details>
