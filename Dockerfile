#### Base ####
FROM golang:1-alpine as base

ENV GOMAXPROCS=4

WORKDIR /usr/src/app
COPY . /usr/src/app/

RUN apk update &&\
    apk upgrade --no-cache &&\
    apk add git --no-cache &&\
    go mod download &&\
    go get github.com/swaggo/swag/cmd/swag &&\
    swag init -o ./src/docs


#### Build ####
FROM base as build

RUN go build -o sso.go

#### Production ####
FROM golang:1-alpine as production

ENV GIN_MODE=release

COPY --from=build /usr/src/app/sso.go /go/sso.go

EXPOSE 6060

ENTRYPOINT ["./sso.go"]


##### Development ####
FROM base

RUN go get github.com/cespare/reflex

EXPOSE 6060

CMD ["reflex", "-c", "/usr/src/app/reflex.conf"]